import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './Styles/stylesheet.css';
import {BrowserRouter} from "react-router-dom";
import {createStore, applyMiddleware} from "redux";
import rootReducer from "./redux/reducer"; 
import {Provider} from "react-redux";
import App from "./Components/App";
import thunk from "redux-thunk";
import { database } from "./database/config";

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(thunk))

ReactDOM.render(<Provider store={store} ><BrowserRouter><App/></BrowserRouter></Provider>, document.getElementById('root'));

// const tasks = ['Go to shopping', 'wash the dishes', 'take a walk'];

// const element = React.createElement('h1', null, 'Hello World!');

// List Elements by using  JSX
/*const element = 
    <div>
        <h1>TASK LIST</h1>
        <ol> 
            { tasks.map((task, index) => <li key = {index}> {task} </li> ) } 
        </ol> 
    </div>
    ReactDOM.render(element, document.getElementById('root')); */

// COMPONENTS USAGE
/*class List extends Component{
    render(){
        return(
        <ol> 
            { this.props.tasks.map((task, index) => <li key = {index}> {task} </li> ) } 
        </ol> 
        )
    }
}

class Title extends Component{
    render(){
        return (
        <h1> {this.props.title} </h1>
        )
    }
}

class Main extends Component{
    render(){
        return <div>
                <Title title = {'Main Colors'}/>
                <List tasks = {['Yellow', 'Blue', 'Red']}/>
                <List tasks = {['Green', 'Orange', 'Purple']}/>
            </div>
    }
}

ReactDOM.render(<Main/>, document.getElementById('root'));*/