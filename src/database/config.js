import * as firebase from "firebase";

// Your web app's Firebase configuration
var config = {
    apiKey: "AIzaSyAjKFEN-5Bi5u_DoogBNKGs693_CIgQA9Q",
    authDomain: "vetclinic-b0d34.firebaseapp.com",
    databaseURL: "https://vetclinic-b0d34-default-rtdb.firebaseio.com",
    projectId: "vetclinic-b0d34",
    storageBucket: "vetclinic-b0d34.appspot.com",
    messagingSenderId: "643221921803",
    appId: "1:643221921803:web:10eed564fb1cc4da60670e"
  };
  // Initialize Firebase
  firebase.initializeApp(config);

  const database = firebase.database()

  export {database}
