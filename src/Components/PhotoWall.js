import React from 'react';
import Photo from './Photo';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// anchor tag & href attribute allows to create hyperlink 

function PhotoWall(props) {
    return <div>
                <Link className = "add-icon" to = "/AddPhoto"> +  </Link>
                <div className = "photo-grid">
                    {props.posts
                        .sort(function(x,y) {
                            return  y.id - x.id
                        })
                        .map((post, index) => <Photo key={index} post={post} {...props} index = {index}/>)}
                    {/*  {props.posts.map((post, index) => {<Photo key = {index} post = {post} {...props} index = {index}/>})}  */}     
                </div>
            </div>
}

PhotoWall.propTypes = { 
    posts: PropTypes.array.isRequired,
    //onRemovePhoto: PropTypes.func.isRequired
}

/* {props.post
    .sort(function(x,y) {
        return y.id - x.id
    })
    .map((post, index) => <Photo key = {index} post = {post} onRemovePhoto = {props.onRemovePhoto}/>)}
 */

/*class PhotoWall extends Component {
    render(){
        return <div className = "photo-grid">
                {this.props.posts.map((post, index) => <Photo key = {index} post = {post}/>)}
            </div>
    }
}*/

export default PhotoWall