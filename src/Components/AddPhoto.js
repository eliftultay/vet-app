import React, { Component } from 'react';
import { Link } from "react-router-dom";


class AddPhoto extends Component{

    constructor(){
        super()
        this.handleSubmit = this.handleSubmit.bind(this )
    }

    handleSubmit(event){
        event.preventDefault();
        const imageLink= event.target.elements.link.value
        const description = event.target.elements.description.value
        const post = {
                id: Number(new Date()), 
                description: description, 
                imageLink: imageLink
            }
        if (imageLink && description) {
            this.props.startAddingPost(post)
            this.props.onHistory.push('/')
        }
    }

    render(){
        return (
            <div>
                {/* <h1> Animal Photo Wall </h1> */}
                <div>
                    <form onSubmit = {this.handleSubmit}  className = "form"> 
                        <input type = "text" placeholder = "ImageLink" name = "link"/>
                        <input type = "text" placeholder = "AnimalDescription" name = "description"/>
                        <button> POST </button>
                    </form>
                </div>

            </div>
        )
    }
}

export default AddPhoto;