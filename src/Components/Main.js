import React, { Component } from 'react';
import { Link, Route } from "react-router-dom";
import PhotoWall from './PhotoWall';
import AddPhoto from './AddPhoto';
import Single from './Single';
import Title from './Title';
import { removePost } from "../redux/actions";

class Main extends Component {
    componentDidMount() {
        this.props.startLoadingComments()
        this.props.startLoadingPosts()
    }

    /* componentDidMount(){
        this.props.removePost(1)
    } */

    render() {
        console.log(this.props.posts)
        return (
            <div>
                <h1>
                    <Link to="/"> Mansur Tultay's Vet Clinic </Link>
                </h1>
                <Route exact path="/" render={() => (
                    <div>
                        {/* <Title title = {'Mansur Tultay\'s Vet Clinic'}/> */}
                        <PhotoWall {...this.props} />
                    </div>
                )} />

                <Route path="/AddPhoto" render={({ history }) => (
                    <AddPhoto {...this.props} onHistory={history} />
                )} />

                <Route path="/single/:id" render={(params) => (
                    <Single {...this.props} {...params} />
                )} />
            </div>
        );
    }
}
pathname:{`/exam/${this.props.exam.key}`},
state: this.props.exam

export default Main

/* function mapStateToProps(state) {
    return{
        posts: state
    }
} */

/*
class Main extends Component{

/*const posts = [{
     id: "0",
     description: "Tiger",
     imageLink: "https://cdn.vox-cdn.com/thumbor/GzQa3VMNyAITTPQU7ZYMfOjg6lQ=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/19873983/GettyImages_137497593.jpg"
     }, {
     id: "1",
     description: "Horse",
     imageLink: "https://cdni.rbth.com/rbthmedia/images/2018.12/original/5c233e7515e9f946f65be43f.jpg"
     }, {
     id: "2",
     description: "Fox",
     imageLink: "https://i.cdn.newsbytesapp.com/images/l158_29421569828142.jpg"
     }] */

// State helps to update accordingly****

/*class Main extends Component{
    constructor(){
        super()
        this.state = {
            posts: [{
                 id: 0,
                 description: "Tiger",
                 imageLink: "https://cdn.vox-cdn.com/thumbor/GzQa3VMNyAITTPQU7ZYMfOjg6lQ=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/19873983/GettyImages_137497593.jpg"
                 }, {
                 id: 1,
                 description: "Horse",
                 imageLink: "https://cdni.rbth.com/rbthmedia/images/2018.12/original/5c233e7515e9f946f65be43f.jpg"
                 }, {
                 id: 2,
                 description: "Fox",
                 imageLink: "https://i.cdn.newsbytesapp.com/images/l158_29421569828142.jpg"
                 }],
            screen: 'photos' // Photos & AddPhotos
        }
        this.removePhoto = this.removePhoto.bind(this);
        //this.navigate = this.navigate.bind(this);
        this.addPhoto = this.addPhoto.bind(this);
    }
    //Pass remove function  to every iteration
    removePhoto(postRemoved){
        console.log(postRemoved.description);
        this.setState((state) => ({
            posts: state.posts.filter(post => post !== postRemoved)
        }))
    }

    addPhoto(postSubmited){
        this.setState((state) => ({
            posts: state.posts.concat([postSubmited])
        }))
    }

    navigate (){
        this.setState({
            screen: 'addPhoto'
        })
    }

    componentDidMount(){
        const data = SimulateFetchFromDatabase()
        this.setState({
            posts: data
        })
    }

    componentDidUpdate(){
        alert('re-render')
    }

    render(){

        return (
            <div>
                <Route exact path="/" render = {() => (
                    <div>
                        <Title title = {'Mansur Tultay\'s Vet Clinic'}/>
                        <PhotoWall posts = {this.state.posts} onRemovePhoto = {this.removePhoto} onNavigate = {this.navigate}/>
                    </div>
                )}/>

                <Route path="/AddPhoto" render = {({history}) => (
                        <AddPhoto onAddPhoto = {(addedPost) => {
                            this.addPhoto(addedPost)
                            history.push('/')
                        }}/>
                )}/>
            </div>
        )

        return ( <div>
            {
                this.state .screen === 'photos' && (
                <div>
                <Title title = {'Mansur Tultay\'s Vet Clinic'}/>
                <PhotoWall posts = {this.state.posts} onRemovePhoto = {this.removePhoto} onNavigate = {this.navigate}/>
                </div>
                )
            }
            {   this.state.screen === 'addPhoto'&& (
                <div>
                    <AddPhoto/>
                </div>
                )
            }
            </div>
        )

    }
}*/

/*function SimulateFetchFromDatabase() {
    return [{
         id: "0",
         description: "Tiger",
         imageLink: "https://cdn.vox-cdn.com/thumbor/GzQa3VMNyAITTPQU7ZYMfOjg6lQ=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/19873983/GettyImages_137497593.jpg"
         }, {
         id: "1",
         description: "Horse",
         imageLink: "https://cdni.rbth.com/rbthmedia/images/2018.12/original/5c233e7515e9f946f65be43f.jpg"
         }, {
         id: "2",
         description: "Fox",
         imageLink: "https://i.cdn.newsbytesapp.com/images/l158_29421569828142.jpg"
         }]

}*/

